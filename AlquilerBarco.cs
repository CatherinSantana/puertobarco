﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamenPuerto
{
    class AlquilerBarco
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string matricula;

        public string Matricula
        {
            get { return matricula; }
            set { matricula = value; }
        }
        private int largometro;

        public int LargoM
        {
            get { return largometro; }
            set { largometro = value; }
        }

        private string aniofabri;

        public string Aniofabri
        {
            get { return aniofabri; }
            set { aniofabri = value; }
        }
        private int potencia;

        public int Potencia
        {
            get { return potencia; }
            set { potencia = value; }
        }
        private string tipo;

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        private int precio;

        public int Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        //public double ModuloFuncion()
        //{
        //    return 0.2 * this.LargoM;
        //}


        public double CalculoAlquiler(Alquiler objeto, Barco embarcacion)
        {
            double ModuloFuncion = (0.2 * embarcacion.Tamaniome);
            return (objeto.DiasAlqui * ModuloFuncion * objeto.Precio);
        }



        public void RegistroEmbarque()
        {
            AlquilerBarco barco = new AlquilerBarco();

            using (var db = new EntityFrameworkContext())
            {
                Console.WriteLine("Ingrese tipo de Barco");
                barco.Tipo =Console.ReadLine();
                Console.WriteLine("ingrese potencia");
                barco.Potencia = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("ingrese la matricula del barco");
                barco.Matricula = Console.ReadLine();
                Console.WriteLine("ingrese la tamaño del barco");
                barco.LargoM = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("ingrese año de fabricacion");
                barco.Aniofabri = Console.ReadLine();
                Console.WriteLine("ingrese el total de dias");
                int dias = int.Parse(Console.ReadLine().ToString());
                //Console.WriteLine("ingrese el total de dias");
                //barco.Dia = Convert.ToInt32(Console.ReadLine());

                //double modulofuncion = (0.2 *barco.Tipo);
                //double moduloFuncion = barco.LargoM * 0.2 * barco.Potencia;
                //double precios = 1.8 * moduloFuncion * dias;

                Console.WriteLine("total a pagar $:"+barco.Precio);
                Alquiler alqui = new Alquiler();
                double ModuloFuncion = (0.2 * barco.LargoM *barco.Potencia);
                TimeSpan ObtenerDias = alqui.FechaI - alqui.Fechaf;
                Console.WriteLine(ObtenerDias);
                Console.WriteLine("Ingrese el numero de dias para obtener el resultado");
                double NumerodeDias;
                NumerodeDias = Convert.ToInt32(Console.ReadLine());
                alqui.Precio = (NumerodeDias * ModuloFuncion * 1.8);
                Console.WriteLine("El precio total a pagar es:  $" + alqui.Precio);




                //var modulofuncion = (0.2 * barco.LargoM);
                //  double Precio= (1.8 *modulofuncion *dias);
                //barco.Precio = Precio;

                //Console.WriteLine("total a pagar $:"+barco.CalculoAlquiler(alquiler, tipo));

                db.Add(barco);
                db.SaveChanges();

            }
            return;
        }
        public void ListaEmbarcacion()
        {
            using (var db = new EntityFrameworkContext())
            {

                Alquiler alqui = new Alquiler();
                AlquilerBarco barcos = new AlquilerBarco();
                List<AlquilerBarco> todoslosbarcos = db.Alquilers.ToList();
                int c = 1;
                foreach (var barco in todoslosbarcos)
                {
                    
                    Console.WriteLine("============== Embarcacion nº " + c + " =============");
                    Console.WriteLine("Tamaño del Barco:  " + barco.LargoM);
                    Console.WriteLine("matricula:  " + barco.Matricula);
                    Console.WriteLine("Año de Fabricacion:  " + barco.Aniofabri);
                    Console.WriteLine("Tipo de barco:  " + barco.Tipo);
                    Console.WriteLine("potencia:  " + barco.Potencia);


                    //double ModuloFuncion = (0.2 * barco.LargoM * barco.Potencia);
                    //TimeSpan ObtenerDias = alqui.FechaI - alqui.Fechaf;
                    //Console.WriteLine(ObtenerDias);
                    //Console.WriteLine("Ingrese el numero de dias para obtener el resultado");
                    //double NumerodeDias;
                    //NumerodeDias = Convert.ToInt32(Console.ReadLine());
                    //alqui.Precio = (NumerodeDias * ModuloFuncion * 1.8);
                    Console.WriteLine("El precio total a pagar es:  $" + alqui.Precio);

                    Console.WriteLine("===================================================================== ");

                    c++;
                }
            }
            return;
        }
        //public void ListaEmbarcacion100()
        //{
        //    using (var db = new EntityFrameworkContext())
        //    {
        //        AlquilerBarco barcos = new AlquilerBarco();



        //        List<AlquilerBarco> todoslosbarcos = db.Alquilers.ToList();
        //        int c = 1;


        //        {
        //            Console.WriteLine("No existe alquiler");

        //            foreach (var barco in todoslosbarcos)
        //            {
        //                Console.WriteLine("Tipo de barco:  " + barco.Tipo);
        //                Console.WriteLine("potencia:  " + barco.Potencia);
        //                Console.WriteLine("============== Embarcacion nº " + c + " =============");
        //                Console.WriteLine("Tamaño del Barco:  " + barco.LargoM);
        //                Console.WriteLine("matricula:  " + barco.Matricula);
        //                Console.WriteLine("Año de Fabricacion:  " + barco.Aniofabri);
        //                Console.WriteLine("Precio:  " + barco.Precio);
        //                Console.WriteLine("===================================================================== ");

        //                c++;
        //            }
        //        }
        //        return;
        //    }
        //}

        public void Eliminar()
        {
            using (var db = new EntityFrameworkContext())
            {
                AlquilerBarco borrar = db.Alquilers.Find(6);
                db.Alquilers.Remove(borrar);
                db.SaveChanges();
            }
        }
    }
}

