﻿6using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamenPuerto
{
    class Alquiler
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int idalquiler;

        public int Alquilers
        {
            get { return idalquiler; }
            set { idalquiler = value; }
        }

        private int diasalqui;

        public int DiasAlqui
        {
            get { return diasalqui; }
            set { diasalqui = value; }
        }
        private double precio;

        public double Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        private int barco;

        public int Barco
        {
            get { return barco; }
            set { barco = value; }
        }
        //private string cliente;

        //public string IdCliente
        //{
        //    get { return cliente; }
        //    set { cliente = value; }
        //}



        private DateTime fechainicial;

        public DateTime FechaI
        {
            get { return fechainicial; }
            set { fechainicial = value; }
        }
        private DateTime fechafinal;

        public DateTime Fechaf
        {
            get { return fechafinal; }
            set { fechafinal = value; }
        }
        private string amarre;

        public string Amarre
        {
            get { return amarre; }
            set { amarre = value; }
        }

        public void RegistrarAlquiler()
        {
            Alquiler alquiler = new Alquiler();
            //AlquilerBarco barco = new AlquilerBarco();

            using (var db = new EntityFrameworkContext())
            {
                Console.WriteLine("ingrese id de Alquiler");
                alquiler.Alquilers = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("ingrese el total de dias");
                alquiler.DiasAlqui = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Posicion de Amarre Dispoibles:" +
                    " Dc1,Dc2,Dc3 " +
                    "escriba el de preferencia a ocupar");
                alquiler.Amarre = Console.ReadLine();
                Console.WriteLine("ingrese el id de Barco ");
                alquiler.Barco = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("total a pagar $:"+alquiler.Precio);
                AlquilerBarco barco = new AlquilerBarco();

                //double ModuloFuncion = (0.2 * barco.LargoM);
                //Precio = (DiasAlqui * ModuloFuncion * 1.8);
                //Console.WriteLine("El valor a pagar es:  $" + alquiler.Precio);

                //alquiler.fechainicial = DateTime.Now;
                //alquiler.fechafinal = DateTime.Now;




                db.Add(alquiler);
                db.SaveChanges();


            }
            return;
        }
        public void ListaAlquiler()
        {
            using (var db = new EntityFrameworkContext())
            {
                Alquiler alquilers = new Alquiler();
                List<Alquiler> listadoreserva = db.Alqui.ToList();
                int c = 1;

                foreach (var alquiler in listadoreserva)
                {
                    //Console.WriteLine("==================== los datos de su reserva son:==================== ");
                    //Console.WriteLine("Cedula del cliente: " + alquiler.IdCliente);
                    Console.WriteLine("=================Reserva Numero: " + c + "====================");
                    Console.WriteLine("Codigo de Reserva: " + alquiler.Alquilers);
                    Console.WriteLine("Fecha de reserva: " + alquiler.fechainicial);
                    Console.WriteLine("lugar de reserva: " + alquiler.Amarre);
                    Console.WriteLine("Codigo de Barco: " + alquiler.Barco);

                    Console.WriteLine("Costo Total: " + alquiler.Precio);
                    Console.WriteLine("Total de dias: " + alquiler.DiasAlqui);
                    Console.WriteLine("===================================================================== ");

                    c++;



                }

            }
            return;

        }
        public void ConsultarAlquiler()
        {
            using (var db = new EntityFrameworkContext())
            {
                //Alquiler alquiler = new Alquiler();
                Console.WriteLine("Ingrese el ID del Alquiler que deseea consultar ");
                Alquilers = Convert.ToInt32(Console.ReadLine());
                var Alqui = (from resultado in db.Alqui
                             where resultado.Alquilers == Alquilers
                             select resultado).FirstOrDefault();

                if (Alqui == null)
                {
                    Console.WriteLine("No existe alquiler");
                }
                else
                {
                    Console.WriteLine("Dias de Alquiler: " + Alqui.DiasAlqui);
                    Console.WriteLine("Amarre: " + Alqui.Amarre);
                    Console.WriteLine("Codigo Barco: " + Alqui.Barco);
                    Console.WriteLine("fechainicial:" + Alqui.fechainicial);
                    return;
                }
            }
        }


        //        public void modificarReserva()
        //        {
        //            //Alquiler alquiler = new Alquiler();
        //            using (var db = new EntityFrameworkContext())
        //            {
        //                Console.WriteLine("Ingrese el ID del Alquiler que deseea modificar ");
        //                Alquilers = Convert.ToInt32(Console.ReadLine());
        //                var alqui = (from resultado in db.Alqui
        //                             where resultado.Alquilers == Alquilers
        //                             select resultado).FirstOrDefault();

        //                if (alqui == null)
        //                {
        //                    Console.WriteLine("No se encontraron datos");

        //                }
        //                else
        //                {
        //                    Console.WriteLine("Datos Encontrados");

        //                    Console.WriteLine("Ingrese la nueva fecha de reserva");
        //                    alqui.Fechaf = Convert.ToDateTime(Console.ReadLine());
        //                    Console.WriteLine("Posicion de Amarre Dispoibles:" +
        //                                    " kli,Rt3,Dc3 " +
        //                                    "escriba el de preferencia para cambiar pocision");
        //                    alqui.Amarre = Console.ReadLine();
        //                    Console.WriteLine("Ingrese el numero de dias");
        //                    alqui.DiasAlqui = Convert.ToInt32(Console.ReadLine());
        //                    Console.WriteLine("Ingrese codigo de barco");
        //                    alqui.Barco = Convert.ToInt32(Console.ReadLine());
        //                    db.SaveChanges();
        //                }
        //            }
        //            return;
        //        }
        //    }
        //}

        //public void EliminarAlquiler()
        //        {
        //            using (var db = new EntityFrameworkContext())
        //            {
        //                //Alquiler alquiler = new Alquiler();
        //                Console.WriteLine("=====================");
        //                Console.WriteLine("Ingrese el numero reservado de amarre  "); Amarre =Console.ReadLine();
        //                Alquiler alquileres = db.Alqui.Find(Amarre);
        //                db.Alqui.Remove(alquileres);
        //                    db.SaveChanges();
        //                }
        //                return;
        //            }
        public void Eliminar()
        {
            using (var db = new EntityFrameworkContext())
            {
                Alquiler borrar = db.Alqui.Find(2);
                db.Alqui.Remove(borrar);
                db.SaveChanges();
            }
        }
    }
}




//}



