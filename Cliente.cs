﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamenPuerto
{
    class Cliente
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string cedula;

        public string Cedula
        {
            get { return cedula; }
            set { cedula= value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre= value; }
        }
        private string apellido;

        public  string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }


        public void RegistroCliente()
        {

            Cliente cliente = new Cliente();
            using (var db = new EntityFrameworkContext())
            {

                Console.WriteLine("ingrese la cedula del cliente");
                cliente.cedula = Console.ReadLine();
                Console.WriteLine("ingrese Nombres del Cliente");
                cliente.nombre = Console.ReadLine();
                Console.WriteLine("ingrese Apellidos de Cliente");
                cliente.apellido = Console.ReadLine();
                db.Add(cliente);
                db.SaveChanges();
            }
            return;
        }
        public void ListaCliente()
        {
            using (var db = new EntityFrameworkContext())
            {
                Cliente clienters = new Cliente();
                List<Cliente> listadocliente = db.Clientes.ToList();
                int c = 1;

                foreach (var cliente in listadocliente)
                {
                    Console.WriteLine("============== Cliente nº " + c + " =============");
                    Console.WriteLine("==================== ==================== ");
                    Console.WriteLine("Cedula del Cliente: " + cliente.cedula);
                    Console.WriteLine(" Nombres del Cliente: " + cliente.nombre);
                    Console.WriteLine("Apellidos del Cliente: " + cliente.apellido);


                    Console.WriteLine("===================================================================== ");

                    c++;

                }

            }

            return;
        }
        //public void ConsultarCliente()
        //{
        //    using (var db = new EntityFrameworkContext())
        //    {
        //        //Alquiler alquiler = new Alquiler();
        //        Console.WriteLine("Ingrese el Numero De Cedula del Cliente que deseea consultar ");
        //        Cedula = Console.ReadLine();
        //        var client = (from resultado in db.Clientes
        //                     where resultado.Cedula == Cedula
        //                     select resultado).FirstOrDefault();

        //        if (client == null)
        //        {
        //            Console.WriteLine("No existe cliente");
        //        }
        //        else
        //        {
        //            Console.WriteLine("Nombre: " + client.Nombre);
        //            Console.WriteLine("Amarre: " + client.Apellido);
        //            Console.WriteLine("Codigo Barco: " + client.Cedula);
        //         Console.WriteLine("===================================================================== ");


        //            return;
        //        }
        //    }
        //}
        public void modificarCliente()
        {
           
            using (var db = new EntityFrameworkContext())
            {
                Console.WriteLine("Ingrese cedula del cliente que deseea modificar ");
                Cedula = Console.ReadLine();
                var modi = (from resultado in db.Clientes
                             where resultado.Cedula == Cedula
                             select resultado).FirstOrDefault();

                if (modi == null)
                {
                    Console.WriteLine("No se encontraron datos");

                }
                else
                {
                    Console.WriteLine("Datos Encontrados");

                    Console.WriteLine("Ingrese Nueva Cedula Del Cliente");
                    modi.Cedula= Console.ReadLine();
                    Console.WriteLine("Ingrese Nuevos Nombres Del Cliente");
                    modi.Nombre = Console.ReadLine();
                    Console.WriteLine("Ingrese Nuevos Apellidos Del Cliente");
                    modi.Apellido = Console.ReadLine();
                   
                    db.SaveChanges();
                }
            }
            return;
        }
        public void Consultar()
        {
            using (var db = new EntityFrameworkContext())
            {
                //{
                //    using (var db = new EntityFrameworkContext())
                //    {
                //        Alquiler borrar = db.Alqui.Find(2);
                //        db.Alqui.Remove(borrar);
                //        db.SaveChanges();
                //    }
                //}
                Console.WriteLine("Ingrese el id  del Cliente que deseea consultar ");
                Id = Convert.ToInt32(Console.ReadLine());
                var client = (from resultado in db.Clientes
                              where resultado.Id == Id
                              select resultado).FirstOrDefault();

                if (client == null)
                {
                    Console.WriteLine("No existe cliente");
                }
                else
                {
                    Console.WriteLine("Nombre: " + client.Nombre);
                    Console.WriteLine("Amarre: " + client.Apellido);
                    Console.WriteLine("Numero De Cedula: " + client.Cedula);
                    Console.WriteLine("===================================================================== ");


                    return;
                }
            }
        }
    }
}


    

