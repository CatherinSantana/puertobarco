﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPuerto
{
    class EntityFrameworkContext : DbContext
    {
        private const string connectionString =
            "Server =.\\CATHERINSANTANA; Database=ExamenPuerto; Trusted_Connection=True";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)

        {
            optionsBuilder.UseSqlServer(connectionString);
        }
        public DbSet<Alquiler> Alqui { get; set; }
        //public DbSet<Reserva> Reservas { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        //public DbSet<Barco> Barcos { get; set; }
        public DbSet<AlquilerBarco> Alquilers { get; set; }

    }
}


